const btn = document.getElementsByClassName('btn');
const arrBtn = Array.prototype.slice.call(btn);
document.addEventListener('keypress', function (event) {
    const key = event.key;
    arrBtn.find(function (elem) {
        if(elem.innerHTML.toUpperCase() === key.toUpperCase()){
            for (let i = 0; i < arrBtn.length; i++) {
                arrBtn[i].classList.remove('active');
            }
            elem.classList.add('active');
        }
    })
});

