let array = [' Дарт Вейдер', 'Люк Скайуокер', ['Йода', 'Бен Кеноби'], ['Оби-Ван Кеноби','Квай-Гон Джинн'], ' Дарт Мол', 'Дарт Сидиус'];
let timer = document.getElementById('timer');
timer.innerHTML = '10 seconds';
let counter = 10;

function createList(arr) {
    let ul = document.createElement('ul');
    document.getElementById('container').appendChild(ul);
    arr.map(function (elem) {
        if (typeof elem == 'object') {
            let liIn = document.createElement('li');
            ul.appendChild(liIn);
            let ulIn = document.createElement('ul');
            liIn.appendChild(ulIn);
            elem.map(function (elemIn) {
                let li = document.createElement('li');
                ulIn.appendChild(li);
                li.innerHTML = li.innerHTML + `Jedi Star Wars: ${elemIn}`;
            })
        } else {
            let li = document.createElement('li');
            ul.appendChild(li);
            li.innerHTML = li.innerHTML + `Star Wars villains: ${elem}`;
        }
    });
    setInterval(function () {
        counter--;
        if (counter < 0) {
            document.body.innerHTML = '';
        } else {
            timer.innerHTML = counter.toString() + ' seconds';
        }
    }, 1000);
}
createList(array);